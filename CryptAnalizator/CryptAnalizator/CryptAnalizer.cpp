#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <bitset>
#include <ctime>
using namespace std;

int DEG(bitset<32> pol) {

    for (int i = 31; i >= 0; i--) {
        if (pol[i] == 1) {
            return i+1;
        }
    }
    return 0;
}

bitset<32> XOR(bitset<32> pol1, bitset<32> pol2) {
    return pol1^pol2;
}

void moduleByPolinom(bitset<32> &pol, bitset<32> mod) {
    
    int polDeg = DEG(pol);
    int modDeg = DEG(mod);
    int k;
    
    if (modDeg > polDeg)
        return;
    
    while (polDeg >= modDeg) {
        k = polDeg - modDeg;
        mod <<= k;
        pol = XOR(pol, mod);
        mod >>= k;
        polDeg = DEG(pol);
    }
    
}

bitset<32> multiplyPolinoms(bitset<32> a, bitset<32> b) {
    int k = (int)a.to_ulong();
    bitset<32> g;
    for (int j = 0; j < 32; j++) {
        if (b[j] == 1) {
            bitset<32> f = (k*pow(2, j));
            g = XOR(g, f);
        }
    }
    return g;
}

bitset<32> gornerScheme(bitset<32> a, bitset<32> m, bitset<32> pol) {
    bitset<32>  b(1), c = a;
    
    for (int i = 0; i<17; i++) {
        if (m[i] == 1) {
            b = multiplyPolinoms(b, c);
            moduleByPolinom(b, pol);
            
        }
        c = multiplyPolinoms(c, c);
        moduleByPolinom(c, pol);
    }
    
    return b;
}

bitset<32>** truthTable(bitset<32> pol, int n, int deg) {
    ofstream table("trueTable.txt");
    int q = pow(2, n);
    bitset<32> **truthTable = new  bitset<32>*[2];
    for (int i = 0; i < 2; i++) {
        truthTable[i] = new bitset<32>[q];
    }
    
    bitset<32> m(deg);
    for (int i = 0; i < q; i++) {
        bitset<32>  temp(i);
        bitset<32> Y = gornerScheme(temp, m, pol);
        truthTable[0][i] = temp;
        truthTable[1][i] = Y;
        table<<truthTable[0][i]<<"\t"<<truthTable[1][i]<<endl;
    }
    return truthTable;
}

void MDP(bitset<32> ** truthTable, int n) {
    
    int maxB = 0;
    bitset<32> bitB;
    int index;
    int maxInFor = 0;
    int max_num = pow(2, n);
    ofstream ofs("MDP.txt");
    
    int* arrDef = new int[max_num];;
    for (int i = 1; i<max_num; i++)
    {
        for (int k = 0; k<max_num; k++)
            arrDef[k] = 0;
        
        for (int j = 0; j<max_num; j++)
        {
            bitB = truthTable[1][j] ^ truthTable[1][i ^ j];
            index = (int)bitB.to_ulong();
            arrDef[index] += 1;
        }
        
        for (int h = 0; h<max_num; h++)
        {
            if (arrDef[h] > maxInFor)
                maxInFor = arrDef[h];
        }
        if (maxInFor > maxB)
            maxB = maxInFor;
        cout << "temp MDP:" << maxB << endl;
    }
    
    double answer = ((double)maxB)/max_num;
    
    cout << "Last MDP ";
    cout << answer << setprecision(20) << endl;;
    ofs << endl << "MDP(F) = " << answer;
    ofs.close();
    
    delete[] arrDef;
}

int** walshCoefficients(bitset<32>** truthTable, int n) {
    int q = pow(2, n);
    int **walshCoef = new  int*[q];
    for (int i = 0; i < q; i++)
        walshCoef[i] = new int[n];
    
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < q; j++) {
            if (truthTable[1][j][i] == 0) {
                walshCoef[j][i] = 1;
            }
            else {
                walshCoef[j][i] = -1;
            }
        }
    }
    // некультурні вислови про розбиванні всіх вхідних значень на пари які відрізняються на один біт
    for (int b = 0; b < n; b++) {
        for (int k = n - 1; k >= 0; k--) {
            for (int i = 0; i < q; i++) {
                bitset<32> temp = truthTable[0][i];
                if (temp[k] == 0){
                    temp[k] = 1;
                    int ind = (int)temp.to_ullong();
                    int u0 = walshCoef[i][b], u1 = walshCoef[ind][b];
                    walshCoef[i][b] = u0 + u1;
                    walshCoef[ind][b] = u0 - u1;
                }
            }
        }
    }
    ofstream ofs("walshCoefficients.txt");
    for (int i = n - 1; i >= 0; i--) {
        for (int j = 0; j < q; j++) {
            ofs << setw(3) << walshCoef[j][i] << "    ";
        }
        ofs << endl;
    }
    
    ofs.close();
    return walshCoef;
}

void nolinearity(int **walshCoef, int n) {
    int q = pow(2, n);
    ofstream ofs("nolinearity.txt");
    
    for (int j = 0; j < n; j++){
        int max = abs(walshCoef[0][j]);
        for (int i = 1; i < q; i++) {
            if (max < abs(walshCoef[i][j]))
                max = abs(walshCoef[i][j]);
        }
        int NL = pow(2, n - 1) - max / 2;
        ofs << "Nolinearity f" << j + 1 << " = " << NL << endl;
    }
    
    ofs.close();
}

void correlationImmunity(bitset<32>** truthTable, int n) {
    int q = pow(2, n);
    ofstream ofs("correlationImmunity.txt");
    
    for (int i = n - 1; i >= 0; i--) {
        
        int tw = 1;
        
        for (int j = 0; j < q; j++) {
            
            if (truthTable[0][j].count() == tw && truthTable[1][j][i] != 0) {
                ofs << tw - 1 << " ";
                break;
            }
            
        }
        
    }
    
    ofs.close();
}

void disbalance(bitset<32>** truthTable, int n, int **walshCoef) {
    int q = pow(2, n);
    ofstream ofs("disbalance.txt");
    int *count = new int[n];
    for (int i = 0; i<n; i++)
        count[i] = 0;
    
    for (int i = 0; i<q; i++) {
        for (int j = 0; j<n; j++) {
            if (truthTable[1][i][j] == 1)
                count[j]++;
        }
    }
    
    for (int i = 0; i<n; i++)
        ofs << "disbalans f" << i + 1 << " = " << abs(count[i] - (q - count[i])) << endl;
    delete[]count;
    ofs.close();
    
    
    /// or check!!!

    ofstream ofsW("disbalanceBaseOnWhalsh.txt");
    int *countW = new int[n];
    for (int i = 0; i < n; i++)
        countW[i] = 0;
    
    
    for (int i = 0; i < n; i++) {
        countW[i] = walshCoef[0][i];
        ofsW << "disbal f" << i + 1 << " = " << countW[i] << endl;
    }
    
    delete []countW;
    ofsW.close();
}

void polinomZhehalkina(bitset<32>** truthTable, int n) {
    ofstream ofs("colinomZhehalkina.txt");
    int q = pow(2, n);
    bitset<32> *coef = new  bitset<32>[q];
    int *max = new  int[n];
    for (int i = 0; i < q; i++)
        coef[i] = truthTable[1][i];
    for (int i = 0; i < n; i++)
        max[i] = 0;
    
    
    for (int k = n - 1; k >= 0; k--) {
        for (int i = 0; i < q; i++) {
            bitset<32> temp = truthTable[0][i];
            if (temp[k] == 0){
                temp[k] = 1;
                int ind = (int)temp.to_ullong();
                coef[ind] = XOR(coef[ind], coef[i]);
            }
        }
    }
    
    for (int j = 0; j < q; j++) {
        int wt = (int)truthTable[0][j].count();
        for (int b = 0; b < n; b++) {
            if (coef[j][b] == 1 && max[b] < wt)
                max[b] = wt;
        }
    }
    
    for (int b = 0; b < n; b++)
        ofs << "deg f" << b + 1 << " = " << max[b] << endl;
    
    ofs.close();
    delete[] max;
    delete[] coef;
}

void coefficientsOfSpreadingErrors(bitset<32>** truthTable, int n) {
    int q = pow(2, n);
    int *K = new int[n];
    ofstream ofs("coefficientsOfSpreadingErrors.txt");
    
    for (int i = 0; i < n; i++) {
        
        for (int j = 0; j < n; j++)
            K[j] = 0;
        bitset<32> e;
        e[i] = 1;
        
        for (int j = 0; j < q; j++) {
            bitset<32> sum = XOR(truthTable[0][j], e);
            bitset<32> y2 = truthTable[1][sum.to_ullong()];
            bitset<32> rez = XOR(truthTable[1][j], y2);
            for (int k = n - 1; k >= 0; k--)
                K[k] += rez[k];
        }
        
        for (int j = 0; j < n; j++)
            ofs << setw(5) << K[j] << "      ";
        ofs << endl;
        for (int j = 0; j < n; j++) {
            double p = pow(2, n - 1);
            double s = (double)abs(K[j] - p);
            double e = s / p;
            ofs << setw(5) << e * 100 << "%     ";
        }
        ofs << endl << endl;
    }
    
    delete[]K;
    ofs.close();
}

void coefficientsOfSpreadingErrorsOnAverage(bitset<32>** truthTable, int n) {
    int q = pow(2, n);
    int K = 0;
    ofstream ofs("coefficientsOfSpreadingErrorsOnAverage.txt");
    
    for (int i = 0; i < n; i++) {
        
        bitset<32> e;
        e[i] = 1;
        for (int j = 0; j < q; j++) {
            bitset<32> x = truthTable[0][j];
            bitset<32> sum = XOR(x, e);
            bitset<32> y1 = truthTable[1][j];
            bitset<32> y2 = truthTable[1][sum.to_ullong()];
            bitset<32> rez = XOR(y1, y2);
            K += rez.count();
        }
        
        ofs << "K" << setw(2) << i + 1 << "(F)" << " = " << setw(4) << K << "     ";
        double a = abs(K - n*pow(2, n - 1));
        double b = n*pow(2, n - 1);
        ofs << "e" << setw(2) << i + 1 << "(F)" << " = " << setw(4) << a / b << endl << endl;
        K = 0;
    }
    
    ofs.close();
}


//-----------


int main() {
    
    clock_t t0 = clock();
    
    bitset<32> my_pol;
    my_pol[0] = 1;
    my_pol[3] = 1;
    my_pol[17] = 1;
    
    int n = 17;
    
    bitset<32>** table = truthTable(my_pol, n, 271); // <--------------
    
    clock_t t1 = clock();
    cout << "Table done, time: " << (double)(t1 - t0) / CLOCKS_PER_SEC << endl;
    
    int**walshCoef = walshCoefficients(table, n);
    
    clock_t t2 = clock();
    cout << "WalshCoefficients done, time: " << (double)(t2 - t1) / CLOCKS_PER_SEC << endl;
    
    polinomZhehalkina(table, n);

    clock_t t3 = clock();
    cout << "PolinomZhehalkina done, time: " << (double)(t3 - t2) / CLOCKS_PER_SEC << endl;
    
    disbalance(table, n, walshCoef);
    
    clock_t t4 = clock();
    cout << "Disbalance done, time: " << (double)(t4 - t3) / CLOCKS_PER_SEC << endl;

    coefficientsOfSpreadingErrorsOnAverage(table, n);
    
    clock_t t5 = clock();
    cout << "CoefficientsOfSpreadingErrorsOnAverage done, time: " << (double)(t5 - t4) / CLOCKS_PER_SEC << endl;

    coefficientsOfSpreadingErrors(table, n);
    
    clock_t t6 = clock();
    cout << "CoefficientsOfSpreadingErrors done, time: " << (double)(t6 - t5) / CLOCKS_PER_SEC << endl;

    correlationImmunity(table, n);
    
    clock_t t7 = clock();
    cout << "CorrelationImmunity done, time: " << (double)(t7 - t6) / CLOCKS_PER_SEC << endl;
 
    nolinearity(walshCoef, n);
    
    clock_t t8 = clock();
    cout << "Nolinearity done, time: " << (double)(t8 - t7) / CLOCKS_PER_SEC << endl;

    MDP(table, n);
    
    clock_t t9 = clock();
    cout << "MaxDifP done, time: " << (double)(t9 - t8) / CLOCKS_PER_SEC << endl;
    cout << "Full time: " << (double)(t9 - t0) / CLOCKS_PER_SEC << endl;

    for (int i = 0; i < 2; i++)
        delete[]table[i];
    delete[]table;
    for (int i = 0; i < pow(2, n); i++)
        delete[]walshCoef[i];
    delete[]walshCoef;

    return 0;
}